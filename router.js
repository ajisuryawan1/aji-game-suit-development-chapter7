const router = require("express").Router();
const home = require("./controllers/homeController");
const user = require("./controllers/userController");
const auth = require("./controllers/authController");
const restrict = require("./middlewares/restrict");
const isAdmin = require("./middlewares/isAdmin");

router.get("/", home.index);
router.get("/game", restrict, home.game);

router.get("/users", restrict, isAdmin, user.index);
router.get("/users/create", isAdmin, restrict, user.create);
router.post("/users", restrict, isAdmin, user.store);
router.get("/users/:id", restrict, isAdmin, user.show);
router.get("/users/:id/edit", restrict, isAdmin, user.edit);
router.post("/users/:id", restrict, isAdmin, user.update);
router.post("/users/:id/delete", restrict, isAdmin, user.destroy);

router.get("/login", (req, res) => res.render("login"));
router.post("/login", auth.login);
router.get("/register", (req, res) => res.render("register"));
router.post("/regiser", auth.register);

module.exports = router;
