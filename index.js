const express = require("express");
const router = require("./router");
const session = require("express-session");
const flash = require("express-flash")
const passport = require("./lib/passport");
const app = express();
const port = 8000;

const logger = (req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
}

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));
app.use(express.static(`${__dirname}/assets`));
app.use(logger);
app.use(session({
    secret: "buat ini jadi rahasia",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(router);

// internal server error handler
app.use((err, req, res, next) => {
    console.log(err);
    res.status(500).json({
        status: "fail",
        errors: err.message,
    });
});

// 404 handler
app.use((req, res, next) => {
    res.status(404).json({
        status: "fail",
        errors: "are you lost?",
    });
});

app.listen(port, () => console.log(`server nyala di http://localhost:${port}`));
